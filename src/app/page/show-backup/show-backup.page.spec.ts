import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowBackupPage } from './show-backup.page';

describe('ShowBackupPage', () => {
  let component: ShowBackupPage;
  let fixture: ComponentFixture<ShowBackupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowBackupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowBackupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
