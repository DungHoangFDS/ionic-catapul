import { Component, OnInit, Renderer, ViewChild, ElementRef } from '@angular/core';
import {IonSlides} from '@ionic/angular';

@Component({
  selector: 'app-create-wallet-guide1',
  templateUrl: './create-wallet-guide1.page.html',
  styleUrls: ['./create-wallet-guide1.page.scss'],
})

export class CreateWalletGuide1Page implements OnInit {
  @ViewChild('slides') slides: IonSlides;
  constructor(
    public renderer: Renderer
  ) { }

  ngOnInit() {
  }
  changeSlide() {
    this.slides.getActiveIndex().then(number => {
      console.log(number);
      
    })
  }
}
