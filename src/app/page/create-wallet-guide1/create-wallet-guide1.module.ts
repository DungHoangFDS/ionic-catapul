import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreateWalletGuide1Page } from './create-wallet-guide1.page';

const routes: Routes = [
  {
    path: '',
    component: CreateWalletGuide1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreateWalletGuide1Page]
})
export class CreateWalletGuide1PageModule {}
