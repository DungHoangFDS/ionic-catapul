import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWalletGuide1Page } from './create-wallet-guide1.page';

describe('CreateWalletGuide1Page', () => {
  let component: CreateWalletGuide1Page;
  let fixture: ComponentFixture<CreateWalletGuide1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWalletGuide1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWalletGuide1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
