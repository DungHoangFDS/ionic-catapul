import { Component, OnInit } from '@angular/core';
import { CalculateAgeService } from 'src/app/service/calculate-age.service';

@Component({
  selector: 'app-term-condition',
  templateUrl: './term-condition.page.html',
  styleUrls: ['./term-condition.page.scss'],
})
export class TermConditionPage implements OnInit {

  constructor(private calculateService: CalculateAgeService) { }

  ngOnInit() {
  }

}
