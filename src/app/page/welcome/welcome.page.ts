import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountHttp } from 'nem2-sdk';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    const accountHttp = new AccountHttp('http://localhost:3000');
  }

  clickAnyWhere() {
    this.router.navigate(['privacy-policy']);
  }
}
