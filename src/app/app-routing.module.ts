import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  { path: 'welcome', loadChildren: './page/welcome/welcome.module#WelcomePageModule' },
  { path: 'privacy-policy', loadChildren: './page/privacy-policy/privacy-policy.module#PrivacyPolicyPageModule' },
  { path: 'term-condition', loadChildren: './page/term-condition/term-condition.module#TermConditionPageModule' },
  { path: 'create-wallet-guide1', loadChildren: './page/create-wallet-guide1/create-wallet-guide1.module#CreateWalletGuide1PageModule' },
  { path: 'create-wallet', loadChildren: './page/create-wallet/create-wallet.module#CreateWalletPageModule' },
  { path: 'create-backup', loadChildren: './page/create-backup/create-backup.module#CreateBackupPageModule' },
  { path: 'show-backup', loadChildren: './page/show-backup/show-backup.module#ShowBackupPageModule' },
  { path: 'create-password', loadChildren: './page/create-password/create-password.module#CreatePasswordPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
